# Mim Music
#### Introduction
Mim Music , play modern music。
#### About
MIM music is a simple music player. It uses bass to play music, comet's desktop lyrics display and fog wood cutting tray module.
Currently under development, you can only listen to relevant music obtained from Netease cloud, and more music sources will be updated in the future.
#### Installation tutorial
You only need to download the MIM main program to run it directly.
#### Instructions for use
After startup, a file will be generated in the directory, system ini、list. JSON et al;
1. system. ini
system. Ini is a related configuration item, which will continue to increase in the future. After deletion, the startup setting will return to the initial state.
2. list. json
list. JSON is a local song list, which is stored in JSON standard format and will be continuously updated later.
3. config. ini
config. Ini is the initial small configuration.
#### Related links
1.  [ https://space.bilibili.com/323611141 ]
2.  [ https://www.bilibili.com/video/BV1vV411n7Le ]